package de.xyaren.xmltv;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import de.xyaren.xmltv.input.VodafoneRepository;
import de.xyaren.xmltv.input.WebserviceUnavailableException;
import de.xyaren.xmltv.output.model.Channel;
import org.ini4j.Ini;
import org.ini4j.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class ChannelListService {
    private static final Logger LOG = LoggerFactory.getLogger(ChannelListService.class);

    @Autowired
    private VodafoneRepository vodafoneRepository;

    @Autowired
    private VodafoneService vodafoneService;

    public void generate(String generate) {
        try {
            File file = new File(generate);

            File parentFile = file.getParentFile();
            if (parentFile != null) {
                parentFile.mkdirs();
            }

            Ini ini = new Ini();
            ini.getConfig().setEscape(false);
            Profile.Section section = ini.add(Constants.INI_MAPPING);
            getChannelList().forEach(channel -> section.add(";" + channel.vodafoneId, channel.name.get(0).name));
            ini.store(file);
        } catch (Exception e) {
            LOG.error("Error during generation occured", e);
        }
    }

    private List<Channel> getChannelList() throws WebserviceUnavailableException {
        JsonArray jsonArray = vodafoneRepository.getChannels();
        ArrayList<Channel> channels1 = new ArrayList<>();

        for (JsonElement channelJsonElement : jsonArray) {
            JsonObject channelJson = channelJsonElement.getAsJsonObject();
            Channel channel = vodafoneService.buildUpChannel(channelJson, null);
            if (channel != null) {
                channels1.add(channel);
            }
        }

        vodafoneService.printChannels(channels1);
        return channels1;
    }

}
