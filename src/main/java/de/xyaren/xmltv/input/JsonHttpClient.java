package de.xyaren.xmltv.input;

import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Map;

@Component
public class JsonHttpClient {
    private static final Logger LOG = LoggerFactory.getLogger(JsonHttpClient.class);

    @Autowired
    private JsonElementMesssageConverter messsageConverter;

    @Retryable(value = WebserviceUnavailableException.class, maxAttempts = 10, backoff = @Backoff(delay = 100, multiplier = 15))
    public ResponseEntity<JsonObject> query(String url, Map<String, Object> args) throws WebserviceUnavailableException {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().clear();
        restTemplate.getMessageConverters().add(0, messsageConverter);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<Void> entity = new HttpEntity<>(null, headers);

        ResponseEntity<JsonObject> exchange = restTemplate.exchange(url, HttpMethod.GET, entity, JsonObject.class, args);

        switch (exchange.getStatusCode().series()) {
            case SERVER_ERROR:
                LOG.warn("{} received.", exchange.getStatusCode().value());
                throw new WebserviceUnavailableException("HTTP " + exchange.getStatusCode().value() + " " + exchange.getStatusCode().getReasonPhrase());
        }

        return exchange;
    }


}
