package de.xyaren.xmltv.input;

public class WebserviceUnavailableException extends Exception {

    public WebserviceUnavailableException() {
        super();
    }

    public WebserviceUnavailableException(String message) {
        super(message);
    }

    public WebserviceUnavailableException(String message, Throwable cause) {
        super(message, cause);
    }

    public WebserviceUnavailableException(Throwable cause) {
        super(cause);
    }
}
