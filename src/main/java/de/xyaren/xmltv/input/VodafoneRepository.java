package de.xyaren.xmltv.input;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class VodafoneRepository {
    private static final String API_URL = "https://tv-manager.vodafone.de/tv-manager/backend/auth-service/proxy/epg-data-service/epg/tv/";
    private static final String REQ_CHANNELS = API_URL + "channels";
    private static final String REQ_CHANNEL = API_URL + "channels/{channel_id}/";
    private static final String REQ_PERIOD = API_URL + "data/period/{channel_ids}/{start}/{end}";
    private static final String REQ_DETAILS = API_URL + "data/item/{itemId}";

    private static final org.joda.time.format.DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss");


    @Autowired
    private JsonHttpClient httpClient;

    public JsonArray getChannels() throws WebserviceUnavailableException {
        return httpClient.query(REQ_CHANNELS, new HashMap<>()).getBody().get("items").getAsJsonArray();
    }

    public JsonArray getProgram(String channelID, LocalDateTime start, LocalDateTime end) throws NoDataException, WebserviceUnavailableException {
        HashMap<String, Object> args = new HashMap<>();
        args.put("channel_ids", channelID);
        args.put("start", formatDate(start));
        args.put("end", formatDate(end));

        JsonObject body = httpClient.query(REQ_PERIOD, args).getBody();

        if (body.has(channelID))
            return body.get(channelID).getAsJsonArray();

        throw new NoDataException("no program received for channel " + channelID);
    }

    public JsonObject getDetails(Long showId) throws WebserviceUnavailableException {
        HashMap<String, Object> args = new HashMap<>();
        args.put("itemId", showId);
        return httpClient.query(REQ_DETAILS, args).getBody();
    }


    private String formatDate(LocalDateTime date) {
        return FORMATTER.print(date);
    }

    public JsonObject getChannel(int channelId) throws NoDataException, WebserviceUnavailableException {
        HashMap<String, Object> args = new HashMap<>();
        args.put("channel_id", channelId);
        ResponseEntity<JsonObject> query = httpClient.query(REQ_CHANNEL, args);

        if (query.getStatusCode().equals(HttpStatus.NO_CONTENT))
            throw new NoDataException("Could not find Channel Data for ID " + channelId);
        return query.getBody();
    }
}
