package de.xyaren.xmltv.input;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.List;

@Component
public class JsonElementMesssageConverter extends AbstractHttpMessageConverter<JsonObject> {

    private JsonParser jsonParser = new JsonParser();

    @Override
    public List<MediaType> getSupportedMediaTypes() {
        return Arrays.asList(MediaType.APPLICATION_JSON_UTF8);
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(JsonObject.class);
    }

    @Override
    protected JsonObject readInternal(Class<? extends JsonObject> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        return jsonParser.parse(new JsonReader(new InputStreamReader(inputMessage.getBody()))).getAsJsonObject();
    }

    @Override
    protected void writeInternal(JsonObject jsonElement, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        JsonWriter jsonWriter = new JsonWriter(new OutputStreamWriter(outputMessage.getBody()));
        jsonWriter.setLenient(true);
        Streams.write(jsonElement, jsonWriter);
    }
}
