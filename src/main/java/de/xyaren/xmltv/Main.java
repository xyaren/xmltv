package de.xyaren.xmltv;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;

@Configuration
@EnableRetry
@ComponentScan
public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    private static final Options OPTIONS = new Options();

    private static final String OPT_GENERATE = "generate";
    private static final String OPT_FETCH = "fetch";
    private static final String OPT_HELP = "help";

    static {
        OPTIONS.addOption(Option.builder("h").longOpt(OPT_HELP).desc("Shows the Help").build());

        OptionGroup modes = new OptionGroup();

        modes.addOption(Option.builder("g").longOpt(OPT_GENERATE).desc("Generate ini file").required().numberOfArgs(1).argName("output file").build());
        modes.addOption(Option.builder("f").longOpt(OPT_FETCH).desc("Fetch data").numberOfArgs(3).argName("ini> <output> <days").required().build());
        OPTIONS.addOptionGroup(modes);
    }

    public static void main(String[] args) throws ParseException {
        try {

            DefaultParser defaultParser = new DefaultParser();
            CommandLine commandLine = defaultParser.parse(OPTIONS, args);

            if (commandLine.getOptions().length == 0 || commandLine.hasOption(OPT_HELP)) {
                printHelp();
            } else {
                ApplicationContext ctx = new AnnotationConfigApplicationContext(Main.class);

                if (commandLine.hasOption(OPT_GENERATE)) {
                    String outputFilePath = commandLine.getOptionValue(OPT_GENERATE);
                    ctx.getBean(ChannelListService.class).generate(outputFilePath);
                } else if (commandLine.hasOption(OPT_FETCH)) {
                    String[] fetches = commandLine.getOptionValues(OPT_FETCH);

                    String ini = fetches[0];
                    String outputPath = fetches[1];
                    int days = Integer.parseInt(fetches[2]);

                    ctx.getBean(VodafoneService.class).fetch(ini, outputPath, days);
                } else {
                    printHelp();
                }
            }
        } catch (ParseException e) {
            System.out.println(e.getLocalizedMessage());
            printHelp();
        }

    }

    private static void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("xmltv.jar", OPTIONS, true);
    }
}
