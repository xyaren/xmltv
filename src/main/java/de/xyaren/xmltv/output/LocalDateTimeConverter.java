package de.xyaren.xmltv.output;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import org.joda.time.ReadableInstant;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

@Component
public class LocalDateTimeConverter implements SingleValueConverter {
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyyMMddHHmmss Z");

    public boolean canConvert(Class clazz) {
        return ReadableInstant.class.isAssignableFrom(clazz);
    }

    @Override
    public String toString(Object obj) {
        return dateTimeFormatter.print((ReadableInstant) obj);
    }

    @Override
    public Object fromString(String str) {
        return dateTimeFormatter.parseDateTime(str);
    }
}
