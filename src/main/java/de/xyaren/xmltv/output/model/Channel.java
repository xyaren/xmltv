package de.xyaren.xmltv.output.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("channel")
public class Channel {

    @XStreamAlias("vodafoneId")
    @XStreamAsAttribute
    public String vodafoneId;

    @XStreamAlias("id")
    @XStreamAsAttribute
    public String externalId;

    @XStreamImplicit
    public List<DisplayName> name;

    @XStreamAlias("icon")
    public Icon icon;
}
