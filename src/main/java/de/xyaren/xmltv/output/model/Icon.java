package de.xyaren.xmltv.output.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("icon")
public class Icon {

    public Icon(String src) {
        this.src = src;
    }

    public Icon(String src, int width, int height) {
        this.src = src;
        this.width = width;
        this.height = height;
    }

    @XStreamAlias("src")
    @XStreamAsAttribute
    public String src;

    @XStreamAlias("width")
    @XStreamAsAttribute
    public Integer width;

    @XStreamAlias("height")
    @XStreamAsAttribute
    public Integer height;
}
