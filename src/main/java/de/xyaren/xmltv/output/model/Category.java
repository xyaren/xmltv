package de.xyaren.xmltv.output.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.converters.extended.ToAttributedValueConverter;

@XStreamAlias("category")
@XStreamConverter(value = ToAttributedValueConverter.class, strings = {"name"})
public class Category {

    public String name;

    @XStreamOmitField
    public int position;
}

