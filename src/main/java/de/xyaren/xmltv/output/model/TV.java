package de.xyaren.xmltv.output.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("tv")
public class TV {

    @XStreamImplicit
    public List<Channel> channels;

    @XStreamImplicit
    public List<Programme> programmes;

}