package de.xyaren.xmltv.output.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.extended.ToAttributedValueConverter;

@XStreamAlias("episode-num")
@XStreamConverter(value = ToAttributedValueConverter.class, strings = {"value"})
public class EpisodeNumber {

    public EpisodeNumber(String system, String value) {
        this.system = system;
        this.value = value;
    }

    @XStreamAlias("system")
    @XStreamAsAttribute
    public String system;

    public String value;

}
