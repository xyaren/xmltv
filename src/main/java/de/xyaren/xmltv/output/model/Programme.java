package de.xyaren.xmltv.output.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import de.xyaren.xmltv.output.CreditConverter;
import org.joda.time.DateTime;

import java.util.List;

@XStreamAlias("programme")
public class Programme {

    @XStreamAlias("channel")
    @XStreamAsAttribute
    public String channel;

    @XStreamAlias("start")
    @XStreamAsAttribute
    public DateTime start;

    @XStreamAlias("stop")
    @XStreamAsAttribute
    public DateTime stop;

    @XStreamAlias("icon")
    public Icon icon;

    @XStreamAlias("date")
    public String date;

    @XStreamAlias("title")
    public Title title;

    @XStreamAlias("country")
    public String country;

    @XStreamAlias("category")
    @XStreamImplicit
    public List<Category> categories;

    @XStreamAlias("sub-title")
    public Title subtitle;

    @XStreamAlias("desc")
    public Title description;

    @XStreamAlias("episode-num")
    @XStreamImplicit
    public List<EpisodeNumber> episodeNumber;

    @XStreamAlias("audio")
    public Audio audio;

    @XStreamAlias("audio")
    public Video video;

    @XStreamAlias("credits")
    @XStreamConverter(CreditConverter.class)
    public List<Credit> credits;

}
