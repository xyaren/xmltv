package de.xyaren.xmltv.output.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("video")
public class Video {

    public Video(String aspect) {
        this.aspect = aspect;
    }

    @XStreamAlias("aspect")
    public String aspect;
}
