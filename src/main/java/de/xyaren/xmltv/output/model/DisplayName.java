package de.xyaren.xmltv.output.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.extended.ToAttributedValueConverter;
import org.apache.commons.lang3.builder.ToStringBuilder;

@XStreamAlias("display-name")
@XStreamConverter(value = ToAttributedValueConverter.class, strings = {"name"})
public class DisplayName {

    public DisplayName(String lang, String name) {
        this.lang = lang;
        this.name = name;
    }

    @XStreamAlias("lang")
    @XStreamAsAttribute
    public String lang;

    public String name;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("lang", lang)
                .append("name", name)
                .toString();
    }
}
