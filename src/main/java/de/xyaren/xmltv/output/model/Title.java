package de.xyaren.xmltv.output.model;

import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.extended.ToAttributedValueConverter;

@XStreamConverter(value = ToAttributedValueConverter.class, strings = {"title"})
public class Title {

    public Title(String title) {
        this.title = title;
    }

    public String title;
}
