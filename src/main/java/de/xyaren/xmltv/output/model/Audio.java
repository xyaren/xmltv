package de.xyaren.xmltv.output.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("audio")
public class Audio {

    public Audio(String stereo) {
        this.stereo = stereo;
    }

    @XStreamAlias("stereo")
    public String stereo;
}
