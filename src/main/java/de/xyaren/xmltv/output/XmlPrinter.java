package de.xyaren.xmltv.output;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import de.xyaren.xmltv.output.model.TV;
import org.apache.commons.io.output.TeeOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.zip.GZIPOutputStream;

@Component
public class XmlPrinter {

    private XStream xStream = new XStream(new DomDriver("UTF-"));

    @Autowired
    private LocalDateTimeConverter localDateTimeConverter;

    @PostConstruct
    public void setUp() {
        xStream.autodetectAnnotations(true);
        xStream.registerConverter(localDateTimeConverter, XStream.PRIORITY_NORMAL);
    }


    public void write(TV obj, String outputPath) throws IOException {

        File gzipedFile = new File(outputPath + ".xml.gz");
        File normalFile = new File(outputPath + ".xml");

        gzipedFile.getParentFile().mkdirs();
        normalFile.getParentFile().mkdirs();

        GZIPOutputStream gzip = new GZIPOutputStream(new FileOutputStream(gzipedFile));
        FileOutputStream normal = new FileOutputStream(normalFile);

        TeeOutputStream tee = new TeeOutputStream(gzip, normal);

//        PrettyPrintWriter printWriter = new PrettyPrintWriter(writer, PrettyPrintWriter.XML_1_0);
        try (OutputStreamWriter writer = new OutputStreamWriter(tee)) {
            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");

//            xStream.marshal(obj, writer);
            xStream.toXML(obj, writer);
            System.out.println("Done.");
        }

    }

}
