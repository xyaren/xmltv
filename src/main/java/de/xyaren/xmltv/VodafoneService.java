package de.xyaren.xmltv;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import de.xyaren.xmltv.input.NoDataException;
import de.xyaren.xmltv.input.VodafoneRepository;
import de.xyaren.xmltv.input.WebserviceUnavailableException;
import de.xyaren.xmltv.output.XmlPrinter;
import de.xyaren.xmltv.output.model.*;
import org.ini4j.Ini;
import org.ini4j.Profile;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class VodafoneService {

    private static final Logger LOG = LoggerFactory.getLogger(VodafoneService.class);

    @Autowired
    private XmlPrinter xmlPrinter;

    @Autowired
    private VodafoneRepository vodafoneRepository;

    private ExecutorService executorService = Executors.newWorkStealingPool(10);


    private static void terminate() {
        LOG.info("Aborting");
        System.exit(0);
    }

    public void fetch(String iniFilePath, String outputPath, int days) {
        Profile.Section mapping = getMapping(iniFilePath);
        LocalDateTime start = LocalDate.now().toLocalDateTime(LocalTime.MIDNIGHT);
        LocalDateTime end = LocalDate.now().plusDays(days - 1).toLocalDateTime(LocalTime.MIDNIGHT.minusMillis(1));

        TV TV = new TV();
        TV.channels = getChannels(mapping);
        TV.programmes = getPrograms(TV.channels, start, end);

        try {
            xmlPrinter.write(TV, outputPath);
        } catch (IOException e) {
            LOG.error("Error writing Output", e);
        }
    }

    private Profile.Section getMapping(String iniFilePath) {
        try {
            Ini ini = new Ini(new File(iniFilePath));
            return ini.get(Constants.INI_MAPPING);
        } catch (IOException e) {
            LOG.error("Could not read ini file", e);
            terminate();
        }
        return null;
    }

    private List<Programme> getPrograms(List<Channel> channels, LocalDateTime start, LocalDateTime end) {
        List<Programme> programmes = Collections.synchronizedList(new ArrayList<>());

        CompletableFuture[] completableFutures = channels.stream()
                .map(channel -> {
                    CompletableFuture<List<Programme>> future = CompletableFuture.supplyAsync(
                            () -> fetchProgramInformationForChannel(channel, start, end),
                            executorService
                    );
                    future.thenAccept(programmes::addAll);
                    return future;
                })
                .toArray(CompletableFuture[]::new);
        CompletableFuture.allOf(completableFutures).join();

//        executorService.shutdown();
        return programmes;
    }

    private List<Channel> getChannels(Profile.Section mapping) {
        List<Channel> channels = Collections.synchronizedList(new ArrayList<>());

        CompletableFuture[] completableFutures = mapping.entrySet().stream()
                .map((Map.Entry<String, String> entry) -> {
                    CompletableFuture<Channel> future = CompletableFuture.supplyAsync(() -> requestChannelInfo(entry.getKey(), entry.getValue()), executorService);
                    future.thenAccept(channels::add);
                    return future;
                })
                .toArray(CompletableFuture[]::new);

        CompletableFuture<Void> voidCompletableFuture = CompletableFuture.allOf(completableFutures);
        try {
            voidCompletableFuture.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            LOG.error("Exception During Fetch of Channel Information", e.getCause().getCause());
        }
        printChannels(channels);
        return channels;
    }

    private Channel requestChannelInfo(String vodafoneID, String tvId) {
        int channelId = Integer.parseInt(vodafoneID);
        try {
            JsonObject channelJson = vodafoneRepository.getChannel(channelId);
            return buildUpChannel(channelJson, tvId);
        } catch (NoDataException | WebserviceUnavailableException e) {
            throw new RuntimeException(e);
        }
    }


    void printChannels(List<Channel> list) {
        for (int i = 0; i < list.size(); i++) {
            Channel channel = list.get(i);
            LOG.info("{}: {} - {}", i, channel.vodafoneId, channel.name.get(0).name);
        }
        LOG.info("Fetched {} Channels", list.size());
    }

    private List<Programme> fetchProgramInformationForChannel(Channel channel, LocalDateTime start, LocalDateTime end) {
        LOG.info("Fetching Shows for Channel {} ({})", channel.vodafoneId, channel.name.get(0));

        try {
            JsonArray shows = vodafoneRepository.getProgram(channel.vodafoneId, start, end);

            LOG.info("Processing {} Shows for Channel {}", shows.size(), channel.vodafoneId);
            ArrayList<Programme> programmes = new ArrayList<>();
            for (JsonElement show : shows) {
                JsonObject showJson = show.getAsJsonObject();
                long id = showJson.get("id").getAsLong();

                try {
                    JsonObject details = vodafoneRepository.getDetails(id);

                    Programme programme = createProgramme(channel, showJson, details);
                    programmes.add(programme);
                } catch (Exception e) {
                    LOG.error("Error fetching Details for Show {}", id, e);
                }
            }
            programmes.sort(Comparator.comparing(value -> value.start));
            return programmes;

        } catch (NoDataException e) {
            LOG.warn("No Programm Data found for {} ({})", channel.name, channel.vodafoneId, e);
        } catch (Exception e) {
            LOG.error("Error fetching Information Channel for {} ({})", channel.name, channel.vodafoneId, e);
        }
        return null;
    }

    Channel buildUpChannel(JsonObject channelJson, String epgID) {
        Channel channel = new Channel();

        channel.name = new ArrayList<>();
        if (channelJson.has("lname")) {
            channel.name.add(new DisplayName("de", channelJson.get("lname").getAsString()));
        }

        channel.vodafoneId = channelJson.get("tvtvid").getAsString();
        if (epgID != null) {
            channel.externalId = epgID;
        } else {
            channel.externalId = channel.vodafoneId;
        }

        if (channelJson.has("logo")) {
            channel.icon = new Icon(Constants.CHANNEL_IMAGE_PREFIX + channelJson.get("logo").getAsString());
        }
        return channel;
    }

    private Programme createProgramme(Channel channel, JsonObject show, JsonObject showDetails) {
        Programme programme = new Programme();

        programme.channel = channel.externalId;


        if (show.has("title")) {
            programme.title = new Title(show.get("title").getAsString());
        }
        if (show.has("image_name")) {
            programme.icon = new Icon(Constants.SHOW_IMAGE_PREFIX + show.get("image_name").getAsString());
        }


        programme.stop = new DateTime(showDetails.get("endDateTimeMillis").getAsLong());
        programme.start = new DateTime(showDetails.get("startDateTimeMillis").getAsLong());

        if (showDetails.has("text_text")) {
            programme.description = new Title(showDetails.get("text_text").getAsString());
        }
        if (showDetails.has("text_short_text")) {
            programme.subtitle = new Title(showDetails.get("text_short_text").getAsString());
        }

        if (showDetails.has("production_year"))
            programme.date = showDetails.get("production_year").getAsString();

        if (showDetails.has("country"))
            programme.country = showDetails.get("country").getAsString();

        fillEpisode(programme, showDetails);

        if (checkBoolean(showDetails, "attribute_stereo")) {
            programme.audio = new Audio("stereo");
        }

        if (checkBoolean(showDetails, "attribute_169")) {
            programme.video = new Video("16:9");
        }

        fillCredits(programme, showDetails);
        fillCategories(programme, showDetails);
        return programme;
    }

    private void fillCategories(Programme programme, JsonObject showDetails) {
        if (showDetails.has("category_tvtv")) {
            programme.categories = new ArrayList<>();
            for (JsonElement category_tvtv : showDetails.get("category_tvtv").getAsJsonArray()) {
                JsonObject categoryJson = category_tvtv.getAsJsonObject();
                Category category = new Category();

                if (categoryJson.has("text")) {
                    category.name = categoryJson.get("text").getAsString();

                    if (categoryJson.has("position")) {
                        category.position = categoryJson.get("position").getAsInt();
                    } else {
                        category.position = Integer.MAX_VALUE;
                    }
                    programme.categories.add(category);
                }
            }
            programme.categories.sort(Comparator.comparingInt(value -> value.position));
        }
    }

    private void fillCredits(Programme programme, JsonObject details) {
        JsonArray personArray = details.get("person").getAsJsonArray();
        if (personArray.size() > 0) {
            programme.credits = new ArrayList<>();
            for (JsonElement jsonElement : personArray) {
                JsonObject obj = jsonElement.getAsJsonObject();
                Credit credit = new Credit();

                credit.name = obj.get("name").getAsString();
                credit.type = obj.get("kind").getAsString();
                credit.pos = obj.get("position").getAsInt();

                programme.credits.add(credit);
            }
            programme.credits.sort(Comparator.comparingInt(value -> value.pos));
            programme.credits.sort(Comparator.comparing(value -> value.type));
        }
    }

    private boolean checkBoolean(JsonObject details, String attribute_stereo) {
        return details.has(attribute_stereo) && details.get(attribute_stereo).getAsBoolean();
    }

    private void fillEpisode(Programme programme, JsonObject details) {
        String relay_key = "relay";
        String episode_key = "series_number";

        if (details.has(episode_key)) {
            programme.episodeNumber = new ArrayList<>();
            int series = details.get(episode_key).getAsInt();

            if (details.has(relay_key)) {
                int relay = details.get(relay_key).getAsInt();
                programme.episodeNumber.add(new EpisodeNumber("onscreen", MessageFormat.format("S{0} E{1}", relay, series)));

            } else {
                programme.episodeNumber.add(new EpisodeNumber("onscreen", MessageFormat.format("E{0}", series)));
            }

        }
    }



}
