package de.xyaren.xmltv;

@SuppressWarnings({"ClassWithoutLogger", "WeakerAccess"})
public class Constants {
    static final String SHOW_IMAGE_PREFIX = "https://tv-manager.vodafone.de/tv-manager/backend/epg_images/";
    static final String CHANNEL_IMAGE_PREFIX = "https://tv-manager.vodafone.de/tv-manager/backend/epg_images_channels/";
    static final String INI_MAPPING = "mapping";
}
